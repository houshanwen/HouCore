﻿using Infrastructure.Attribute;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HouCore.Model.System;

namespace HouCore.Repository.System
{
    [AppService(ServiceLifetime = LifeTime.Transient)]
    public class SysTasksQzRepository: BaseRepository<SysTasksQz>
    {
    }
}
