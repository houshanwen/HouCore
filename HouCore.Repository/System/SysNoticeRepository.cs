using System;
using Infrastructure.Attribute;
using HouCore.Repository.System;
using HouCore.Model.Models;

namespace HouCore.Repository.System
{
    /// <summary>
    /// 通知公告表仓储
    ///
    /// @author houcore
    /// @date 2021-12-15
    /// </summary>
    [AppService(ServiceLifetime = LifeTime.Transient)]
    public class SysNoticeRepository : BaseRepository<SysNotice>
    {
        #region 业务逻辑代码
        #endregion
    }
}