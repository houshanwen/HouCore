using System;
using Infrastructure.Attribute;
using HouCore.Repository.System;
using HouCore.Model.Models;
using HouCore.Model.System;

namespace HouCore.Repository.System
{
    /// <summary>
    /// 文件存储仓储
    ///
    /// @author zz
    /// @date 2021-12-15
    /// </summary>
    [AppService(ServiceLifetime = LifeTime.Transient)]
    public class SysFileRepository : BaseRepository<SysFile>
    {
        #region 业务逻辑代码
        #endregion
    }
}