﻿using Infrastructure.Attribute;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HouCore.Model.System.Dto;
using HouCore.Model.System;

namespace HouCore.Repository.System
{
    /// <summary>
    /// 文章管理
    /// </summary>
    [AppService(ServiceLifetime = LifeTime.Transient)]
    public class ArticleRepository : BaseRepository<Article>
    {
        
    }

    /// <summary>
    /// 文章目录
    /// </summary>
    [AppService(ServiceLifetime = LifeTime.Transient)]
    public class ArticleCategoryRepository : BaseRepository<ArticleCategory>
    {

    }
}
