﻿
namespace Infrastructure
{
    /// <summary>
    /// 获取配置文件POCO实体类
    /// </summary>
    public class OptionsSetting
    {
        public static string ConnAdmin = "conn_houcoreAdmin";
        public static string ConnDbType = "conn_houcoreAdmin_type";
        public static string ConnBus = "conn_bus";
        public static string ConnBusDbType = "conn_bus_type";

        public static string DbKey = "DbKey"; 
        public string Database { get; set; }
        /// <summary>
        /// 是否演示模式
        /// </summary>
        public bool DemoMode { get; set; }
        public MailOptions MailOptions { get; set; }
        public Upload Upload { get; set; }
        public ALYUN_OCS ALYUN_OCS { get; set; }
        public JwtSettings JwtSettings { get; set; }
        public static RedisOptions RedisOptions { get; set; }
        public static RabbitMQ RabbitMQ { get; set; }
        public static EventBus EventBus { get; set; }
    }

    /// <summary>
    /// 发送邮件数据配置
    /// </summary>
    public class MailOptions
    {
        public string From { get; set; }
        public string Password { get; set; }
        public string Smtp { get; set; }
        public int Port { get; set; }
    }
    /// <summary>
    /// 上传
    /// </summary>
    public class Upload
    {
        public string UploadDirectory { get; set; }
        public string UploadUrl { get; set; }
    }
    /// <summary>
    /// 阿里云存储
    /// </summary>
    public class ALYUN_OCS
    {
        public string REGIONID { get; set; }
        public string KEY { get; set; }
        public string SECRET { get; set; }
    }

    /// <summary>
    /// Jwt
    /// </summary>
    public class JwtSettings
    {
        /// <summary>
        /// token是谁颁发的
        /// </summary>
        public string Issuer { get; set; }
        /// <summary>
        /// token可以给那些客户端使用
        /// </summary>
        public string Audience { get; set; }
        /// <summary>
        /// 加密的key（SecretKey必须大于16个,是大于，不是大于等于）
        /// </summary>
        public string SecretKey { get; set; }
        /// <summary>
        /// token时间（分）
        /// </summary>
        public int Expire { get; set; } = 1440;
    }
    
    /// <summary>
    /// Redis服务配置
    /// </summary>
    public class RedisOptions
    { 
        public bool IsEnable { get; set; }

        public string Cache { get; set; }

        public string Session { get; set; }
    }

    /// <summary>
    /// RabbitMQ 服务配置
    /// </summary>
    public class RabbitMQ
    { 
        public bool Enabled { get; set; }

        public string HostName { get; set; }

        public string Port { get; set; }

        public string UserName { get; set; }

        public string Password { get; set; }

        public string RetryCount { get; set; }
    }

    /// <summary>
    /// 事件总线配置
    /// </summary>
    public class EventBus
    {
        public bool Enabled { get; set; }

        public string SubscriptionClientName { get; set; }

        public bool RedisSubscriptionEnabled { get; set; }
    }
}
