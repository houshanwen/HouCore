﻿using HouCore.EventBus.Eventsbus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HouCore.EventBus.Subscriptions
{
    /// <summary>
    /// 事件总线订阅管理器
    /// 接口
    /// </summary>
    public interface IEventBusSubscriptionsManager
    {
        /// <summary>
        /// 标识是否有需要处理的事件
        /// </summary>
        bool IsEmpty { get; }

        /// <summary>
        /// 事件移除方法委托
        /// </summary>
        event EventHandler<string> OnEventRemoved;

        /// <summary>
        /// 添加动态类型订阅
        /// </summary>
        /// <typeparam name="TH"></typeparam>
        /// <param name="eventName"></param>
        void AddDynamicSubscription<TH>(string eventName)
            where TH : IDynamicIntegrationEventHandler;

        /// <summary>
        /// 添加事件订阅
        /// </summary>
        /// <typeparam name="T">事件</typeparam>
        /// <typeparam name="TH">事件处理器</typeparam>
        void AddSubscription<T, TH>()
            where T : IntegrationEvent
            where TH : IIntegrationEventHandler<T>;

        /// <summary>
        /// 移除事件订阅
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="TH"></typeparam>
        void RemoveSubscription<T, TH>()
             where T : IntegrationEvent
             where TH : IIntegrationEventHandler<T>;

        /// <summary>
        /// 移除动态事件订阅
        /// </summary>
        /// <param name="eventName">事件名称</param>
        /// <typeparam name="TH"></typeparam>
        void RemoveDynamicSubscription<TH>(string eventName)
             where TH : IDynamicIntegrationEventHandler;

        /// <summary>
        /// 根据事件类型标识是否有订阅指定事件
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        bool HasSubscriptionForEvent<T>() where T : IntegrationEvent;

        /// <summary>
        /// 根据事件名称标识是否有订阅指定事件
        /// </summary>
        /// <param name="eventName">事件名称</param>
        /// <returns></returns>
        bool HasSubscriptionForEvent(string eventName);

        /// <summary>
        /// 根据事件名称获取事件类型
        /// </summary>
        /// <param name="eventName"></param>
        /// <returns></returns>
        Type GetEventTypeByName(string eventName);

        /// <summary>
        /// 获取指定事件的处理器（消费者）
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        IEnumerable<SubscriptionInfo> GetHandlersForEvent<T>() where T : IntegrationEvent;

        /// <summary>
        /// 根据事件名称获取事件处理器（消费者）
        /// </summary>
        /// <param name="eventName"></param>
        /// <returns></returns>
        IEnumerable<SubscriptionInfo> GetHandlerForEvent(string eventName);

        /// <summary>
        /// 获取指定类型事件名称
        /// </summary>
        /// <typeparam name="T">事件</typeparam>
        /// <returns></returns>
        string GetEventName<T>();

        string GetEventKey<T>();

        /// <summary>
        /// 清除事件处理器（消费者）
        /// </summary>
        void Clear();
    }
}
