﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HouCore.EventBus.Subscriptions
{
    /// <summary>
    /// 订阅信息
    /// </summary>
    public class SubscriptionInfo
    {
        /// <summary>
        /// 标识订阅的事件是否为动态类型
        /// </summary>
        public bool IsDynamic { get; }

        /// <summary>
        /// 处理器类型
        /// </summary>
        public Type HandlerType { get; }

        public SubscriptionInfo(bool isDynamic, Type handlerType)
        {
            IsDynamic = IsDynamic;
            HandlerType = handlerType;
        }

        /// <summary>
        /// 生成动态类型订阅信息
        /// </summary>
        /// <param name="handlerType"></param>
        /// <returns></returns>
        public static SubscriptionInfo Dynamic(Type handlerType)
        {
            return new SubscriptionInfo(true, handlerType);
        }

        /// <summary>
        /// 生成订阅信息
        /// </summary>
        /// <param name="handlerType"></param>
        /// <returns></returns>
        public static SubscriptionInfo Typed(Type handlerType)
        {
            return new SubscriptionInfo(false, handlerType);
        }
    }
}
