﻿using HouCore.EventBus.Eventsbus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HouCore.Admin.WebApi.EventHandling
{
    public class NoticeIntegrationEvent : IntegrationEvent
    {
        public string NoticeId { get; private set; }

        public NoticeIntegrationEvent(string noticeId)
            => NoticeId = noticeId;
    }
}
