﻿using HouCore.EventBus.Eventsbus;
using HouCore.Service.System.IService;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HouCore.Admin.WebApi.EventHandling
{
    public class NoticeIntegrationEventHandler : IIntegrationEventHandler<NoticeIntegrationEvent>
    {
        private readonly ISysNoticeService _SysNoticeService;
        private readonly ILogger<NoticeIntegrationEventHandler> _logger;

        public NoticeIntegrationEventHandler(ISysNoticeService sysNoticeService, ILogger<NoticeIntegrationEventHandler> logger)
        {
            _SysNoticeService = sysNoticeService;
            _logger = logger;
        }

        public async Task Handle(NoticeIntegrationEvent @event)
        {
            _logger.LogInformation("----- Handling integration event: {IntegrationEventId} at {AppName} - ({@IntegrationEvent})", @event.Id, "Blog.Core", @event);

            await _SysNoticeService.QueryById(@event.NoticeId.ToString());
        }
    }
}
