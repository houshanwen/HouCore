﻿using HouCore.Admin.WebApi.Filters;
using HouCore.Admin.WebApi.Hubs;
using HouCore.Service.System.IService;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HouCore.Admin.WebApi.Controllers.System
{
    [Route("Info")]
    [ApiController]
    public class PushMessageController : BaseController
    {
        private readonly ISysNoticeService _SysNoticeService;
        private readonly IHubContext<ChatHub> _chatClient;

        public PushMessageController(ISysNoticeService SysNoticeService, IHubContext<ChatHub> chatClient)
        {
            _SysNoticeService = SysNoticeService;
            _chatClient = chatClient;
        }

        [HttpGet("SendNotice")]
        [ActionPermissionFilter(Permission = "info:info:list")]
        public async Task<IActionResult> SendNotice()
        {
            var list = _SysNoticeService.GetAll();

            await _chatClient.Clients.All.SendAsync("SendMessage", list);
            return SUCCESS(list);
        }
    }
}
