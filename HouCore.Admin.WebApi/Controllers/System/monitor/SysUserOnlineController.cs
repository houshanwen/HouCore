﻿using Microsoft.AspNetCore.Mvc;
using HouCore.Admin.WebApi.Filters;

namespace HouCore.Admin.WebApi.Controllers.monitor
{
    [Verify]
    [Route("monitor/online")]
    public class SysUserOnlineController : BaseController
    {
        [HttpGet("list")]
        public IActionResult Index()
        {
            return SUCCESS(null);
        }
    }
}
