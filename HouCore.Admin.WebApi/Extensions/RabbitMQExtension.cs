﻿using HouCore.EventBus.RabbitMQ;
using Infrastructure;
using Infrastructure.Extensions;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using RabbitMQ.Client;
using System;


namespace HouCore.Admin.WebApi.Extensions
{
    public static class RabbitMQExtension
    {
        public static void AddRabbitMQ(this IServiceCollection service)
        {
            if (service == null) throw new ArgumentNullException(nameof(service));

            if (AppSettings.GetConfig("RabbitMQ:Enabled").ParseToBool())
            {
                service.AddSingleton<IRabbitMQPersistentConnection>(sp =>
                {
                    var logger = sp.GetRequiredService<ILogger<RabbitMQPersistentConnection>>();
                    var factory = new ConnectionFactory()
                    {
                        HostName = AppSettings.GetConfig("RabbitMQ:HostName"),
                        DispatchConsumersAsync = true
                    };

                    if (!string.IsNullOrEmpty(AppSettings.GetConfig("RabbitMQ:UserName")))
                    {
                        factory.UserName = AppSettings.GetConfig("RabbitMQ:UserName");
                    }

                    if (!string.IsNullOrEmpty(AppSettings.GetConfig("RabbitMQ:Password")))
                    {
                        factory.Password = AppSettings.GetConfig("RabbitMQ:Password");
                    }

                    if (!string.IsNullOrEmpty(AppSettings.GetConfig("RabbitMQ:Port")))
                    {
                        factory.Port = AppSettings.GetConfig("RabbitMQ:Port").ParseToInt();
                    }

                    var retryCount = 5;
                    if (!string.IsNullOrEmpty(AppSettings.GetConfig("RabbitMQ:RetryCount")))
                    {
                        retryCount = AppSettings.GetConfig("RabbitMQ:RetryCount").ParseToInt();
                    }

                    return new RabbitMQPersistentConnection(factory, logger, retryCount);
                });
            }

        }
    }
}
