﻿using Autofac;
using HouCore.Admin.WebApi.EventHandling;
using HouCore.EventBus.Eventsbus;
using HouCore.EventBus.RabbitMQ;
using HouCore.EventBus.Subscriptions;
using Infrastructure;
using Infrastructure.Extensions;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HouCore.Admin.WebApi.Extensions
{
    public static class EventBusExtension
    {
        public static void AddEventBus(this IServiceCollection service) 
        {
            if (service == null) throw new ArgumentNullException(nameof(service));

            if (AppSettings.GetConfig("EventBus:Enabled").ParseToBool())
            {
                var subscriptionClientName = OptionsSetting.EventBus.SubscriptionClientName;

                if (AppSettings.GetConfig("EventBus:RedisSubscriptionEnabled").ParseToBool()) //使用Redis进行订阅管理
                {
                    service.AddSingleton<IEventBusSubscriptionsManager, RedisEventBusSubscriptionsManager>();
                }
                else  //使用内存进行订阅管理
                {
                    service.AddSingleton<IEventBusSubscriptionsManager, InMemoryEventBusSubscriptionsManager>();
                }
                service.AddTransient<NoticeIntegrationEventHandler>();

                if (AppSettings.GetConfig("RabbitMQ:Enabled").ParseToBool())
                {
                    service.AddSingleton<IEventBus, EventBusRabbitMQ>(sp =>
                    {
                        var rabbitMQPersistentConnection = sp.GetRequiredService<IRabbitMQPersistentConnection>();
                        var iLifetimeScope = sp.GetRequiredService<ILifetimeScope>();
                        var logger = sp.GetRequiredService<ILogger<EventBusRabbitMQ>>();
                        var eventBusSubcriptionsManager = sp.GetRequiredService<IEventBusSubscriptionsManager>();

                        return new EventBusRabbitMQ(rabbitMQPersistentConnection, logger, iLifetimeScope, eventBusSubcriptionsManager, subscriptionClientName);
                    });
                }

            }
        }
        public static void ConfigureEventBus(this IApplicationBuilder app)
        {
            if (AppSettings.GetConfig("EventBus:Enabled").ParseToBool())
            {
                var eventBus = app.ApplicationServices.GetRequiredService<IEventBus>();

                eventBus.Subscribe<NoticeIntegrationEvent, NoticeIntegrationEventHandler>();
            }
        }
    }
}
