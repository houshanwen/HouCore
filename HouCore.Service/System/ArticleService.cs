﻿using Infrastructure.Attribute;
using SqlSugar;
using HouCore.Model.System;
using HouCore.Repository.System;
using HouCore.Service.System.IService;

namespace HouCore.Service.System
{
    /// <summary>
    /// 
    /// </summary>
    [AppService(ServiceType = typeof(IArticleService), ServiceLifetime = LifeTime.Transient)]
    public class ArticleService : BaseService<Article>, IArticleService
    {
    }
}
