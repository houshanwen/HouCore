﻿using Infrastructure.Attribute;
using HouCore.Model.System;
using HouCore.Repository.System;
using HouCore.Service.System.IService;

namespace HouCore.Service.System
{
    /// <summary>
    /// 定时任务
    /// </summary>
    [AppService(ServiceType = typeof(ISysTasksQzService), ServiceLifetime = LifeTime.Transient)]
    public class SysTasksQzService : BaseService<SysTasksQz>, ISysTasksQzService
    {
        public SysTasksQzService(SysTasksQzRepository repository)
        {
        }
    }
}
