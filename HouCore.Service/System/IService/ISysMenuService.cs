﻿using System.Collections.Generic;
using HouCore.Model.System.Dto;
using HouCore.Model.System;
using HouCore.Model.System.Vo;
using HouCore.Model.Vo.System;

namespace HouCore.Service.System.IService
{
    public interface ISysMenuService
    {
        //List<SysMenu> SelectMenuList(long userId);

        List<SysMenu> SelectMenuList(SysMenu menu, long userId);
        List<SysMenu> SelectTreeMenuList(SysMenu menu, long userId);

        SysMenu GetMenuByMenuId(int menuId);
        int AddMenu(SysMenu menu);

        int EditMenu(SysMenu menu);

        int DeleteMenuById(int menuId);

        string CheckMenuNameUnique(SysMenu menu);

        int ChangeSortMenu(MenuDto menuDto);

        bool HasChildByMenuId(long menuId);

        List<SysMenu> SelectMenuTreeByUserId(long userId);

        List<SysMenu> SelectMenuPermsListByUserId(long userId);

        List<string> SelectMenuPermsByUserId(long userId);

        bool CheckMenuExistRole(long menuId);

        List<RouterVo> BuildMenus(List<SysMenu> menus);

        List<TreeSelectVo> BuildMenuTreeSelect(List<SysMenu> menus);
    }
}
