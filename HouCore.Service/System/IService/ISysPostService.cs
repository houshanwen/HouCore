﻿using System;
using System.Collections.Generic;
using System.Text;
using HouCore.Model.System;
using HouCore.Repository;

namespace HouCore.Service.System.IService
{
    public interface ISysPostService : IBaseService<SysPost>
    {
        string CheckPostNameUnique(SysPost sysPost);
        string CheckPostCodeUnique(SysPost sysPost);
        List<SysPost> GetAll();
    }
}
