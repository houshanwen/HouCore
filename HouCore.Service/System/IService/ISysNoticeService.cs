using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using HouCore.Model.Models;

namespace HouCore.Service.System.IService
{
    /// <summary>
    /// 通知公告表service接口
    ///
    /// @author houcore
    /// @date 2021-12-15
    /// </summary>
    public interface ISysNoticeService: IBaseService<SysNotice>
    {
        List<SysNotice> GetSysNotices();

        Task<SysNotice> QueryById(string id);
    }
}
