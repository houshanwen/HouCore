﻿using HouCore.Model.System;
using HouCore.Repository;

namespace HouCore.Service.System.IService
{
    public interface ISysTasksLogService : IBaseService<SysTasksLog>
    {
        /// <summary>
        /// 记录任务执行日志
        /// </summary>
        /// <returns></returns>
        //public int AddTaskLog(string jobId);
        SysTasksLog AddTaskLog(string jobId, SysTasksLog tasksLog);
    }
}
