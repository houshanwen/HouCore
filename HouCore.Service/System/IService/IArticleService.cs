﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HouCore.Model.System.Dto;
using HouCore.Model.System;

namespace HouCore.Service.System.IService
{
    public interface IArticleService : IBaseService<Article>
    {

    }
}
